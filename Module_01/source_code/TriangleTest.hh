// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  NAME:       TriangleTest.hh
//
//  PROJECT:    [ Module 1 ] Triangle Test
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    To provide the header-definition of this program for determining
//              if the 3 values passed construct a Scalene, Isosceles or
//              Equilateral triangle.
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  Feb 3, 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#ifndef TRIANGLETEST_HH_
#define TRIANGLETEST_HH_

#include <string>

class TriangleTest
{
    public:
        TriangleTest();

        virtual
        ~TriangleTest();

        std::string
        printResult(const int, const int, const int);

    private:
        bool
        isIsosceles(const int, const int, const int);

        bool
        isScalene(const int, const int, const int);

        bool
        isEquilateral(const int, const int, const int);

        bool
        isValidRange(const int);
};

#endif /* TRIANGLETEST_HH_ */
