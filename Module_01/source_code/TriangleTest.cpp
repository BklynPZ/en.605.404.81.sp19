// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  NAME:		TriangleTest.cpp
//
//  PROJECT:    [ Module 1 ] Triangle Test
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    To provide the main execution point for this program.
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  Feb 3, 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#include <iostream>

#include "TriangleTest.hh"

int main(int argc, char *argv[]) {
    /***************************************************************************
     * Function: main()
     * Purpose:  provides the main execution point for this program
     * Inputs:   parameters passed from the console
     * Outputs:  integer result: 0 = success, 1 = error, 2 = missing input params
     **************************************************************************/
    int success = 1;    // set for unfavorable outcome

    if (argc == 4) {

        int x, y, z = 0;

        TriangleTest obj;
        std::cout << obj.printResult(x, y, z) << std::endl;

        success = 0;    // sets for a favorable outcome (a message prints to console)
    }
    else {
        success = 2;    // insufficient inputs
        std::cerr
                << "ERROR: Insufficient inputs provided, we expected 3 integers!!!"
                << std::endl;
    }

    std::cout << "Program Done!" << std::endl;  // so we'll know when we're done.

    return (success);
}



