// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  FILE NAME:  TriangleTest.cxx
//
//  PROJECT:    [ Module 1 ] Triangle Test
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    To provide the implementation methods of this program for
//              determining if the 3 values passed construct a Scalene, Isosceles
//              or Equilateral triangle.
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  Feb 3, 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#include <string>
#include "TriangleTest.hh"

TriangleTest::TriangleTest() {
    /***************************************************************************
     * Method:  TriangleTest()::public
     * Purpose: default constructor for the class
     * Inputs:  None
     * Outputs: Nothing
     **************************************************************************/

}

TriangleTest::~TriangleTest() {
    /***************************************************************************
     * Method:  ~TriangleTest()::public
     * Purpose: defult destructor for the class
     * Inputs:  None
     * Outputs: Nothing
     **************************************************************************/

}

bool TriangleTest::isIsosceles(const int x, const int y, const int z) {
    /***************************************************************************
     * Method:  isIsosceles()::private
     * Purpose: tests values to determine if lengths similar to an Isosceles triangle
     *          where 2 of 3 lengths are equal.
     * Inputs:  3 integer values representing the side of a triangle
     * Outputs: boolean result (True|False)
     **************************************************************************/
    bool result = false;

    if (x == y || x == z || y == z) {
        result = true;
    }

    return ( result );
}

bool TriangleTest::isScalene(const int x, const int y, const int z) {
    /***************************************************************************
     * Method:  isScalene()::private
     * Purpose: tests values to determine if lengths similar to a Scalene triangle
     *          where no side is equal.
     * Inputs:  3 integer values representing the side of a triangle
     * Outputs: boolean result (True|False)
     **************************************************************************/
    bool result = false;

    if (x != y || y != z || x != z) {
        result = true;
    }

    return ( result );
}

bool TriangleTest::isEquilateral(const int x, const int y, const int z) {
    /***************************************************************************
     * Method:  isEquilateral()::private
     * Purpose: tests values to determine if lengths equal an Equilateral triangle
     *          where all 3 sides are equal to each other.
     * Inputs:  3 integer values representing the side of a triangle
     * Outputs: boolean result (True|False)
     **************************************************************************/
    bool result = false;

    if (x == y && y == z) {
        result = true;
    }

    return ( result );
}

bool TriangleTest::isValidRange(const int input){
    /***************************************************************************
     * Method:  isValid()::private
     * Purpose: To test and ensure that the value is greater than 0
     * Inputs:  integer value (x, y, or z)
     * Outputs: boolean result (True|False)
     **************************************************************************/
    bool result = false;

    if (input > 0) {
        result = true;
    }
    return ( result );
}

std::string TriangleTest::printResult( const int x, const int y, const int z){
    /***************************************************************************
     * Method:  printReults()::public
     * Purpose: drives a series of checks to determine the relationships between
     *          the numbers passed an a well-known triangle
     * Inputs:  3 integer values representing the side of a triangle
     * Outputs: formatted string of result
     **************************************************************************/
    std::string result;

    if (this->isValidRange(x) and this->isValidRange(y) and this->isValidRange(z)){

        if (this->isEquilateral(x, y, z)) {
            result =
                    "This is an Equilateral Triangle.";
        }
        else if (this->isIsosceles(x, y, z)) {
            result =
                    "This is an Isosceles Triangle.";
        }
        else if (this->isScalene(x, y, z)) {
            result =
                    "This is a Scalene Triangle.";
        }
    }
    else {
        result = "WARNING: We expected you to provide 3 non-negative integers.\n";
    }

    return ( result );
}
