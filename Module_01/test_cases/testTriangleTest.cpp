// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  NAME:       TriangleTest.cpp
//
//  PROJECT:    [ Module 1 ] Triangle Test
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    Provides a suite of tests to exercise the Unit Under Test (UUT).
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  Feb 3, 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#include <string>
#include <iostream>
#include <cstdlib>

#include "../source_code/TriangleTest.hh"

bool testForInvalidInput_X(const TriangleTest &uut, int x, int y, int z) {
    /***************************************************************************
     * Function:  testForInvalidInput_X()
     * Purpose:   tests to see if the value of x is greater than 0.
     * Inputs:    referenced object, with 3 integer value
     * Outputs:   boolean result (True|False)
     **************************************************************************/
    bool success = true;

    std::string actual;
    std::string expected;

    actual = uut.printResult(x,y,z);
    expected = "WARNING: We expected you to provide 3 non-negative integers.";

    if (actual.compare(expected) != 0){
        success = false;
    }

    return ( success );
}

bool testForInvalidInput_Y(const TriangleTest &uut, int x, int y, int z) {
    /***************************************************************************
     * Function:  testForScaleneTriangle()
     * Purpose:   tests to see if the value of y is greater than 0.
     * Inputs:    referenced object, with 3 integer value
     * Outputs:   boolean result (True|False)
     **************************************************************************/
    bool success = true;)

    std::string actual;
    std::string expected;

    actual = uut.printResult(x,y,z);
    expected = "WARNING: We expected you to provide 3 non-negative integers.";

    if (actual.compare(expected) != 0){
        success = false;
    }

    return ( success );
}

bool testForInvalidInput_Z(const TriangleTest &uut, int x, int y, int z) {
    /***************************************************************************
     * Function:  testForScaleneTriangle()
     * Purpose:   tests to see if the value of z is greater than 0.
     * Inputs:    referenced object, with 3 integer value
     * Outputs:   boolean result (True|False)
     **************************************************************************/
    bool success = true;

    std::string actual;
    std::string expected;

    actual = uut.printResult(x,y,z);
    expected = "WARNING: We expected you to provide 3 non-negative integers.";

    if (actual.compare(expected) != 0){
        success = false;
    }

    return ( success );
}

bool testForEquilateralTriangle(TriangleTest &uut, int x, int y, int z) {
    /***************************************************************************
     * Function:  testForScaleneTriangle()
     * Purpose:   tests to see if the values passed are similar to Equilateral
     * Inputs:    referenced object, with 3 integer value
     * Outputs:   boolean result (True|False)
     **************************************************************************/
    bool success = true;

    std::string actual;
    std::string expected;

    actual = uut.printResult(x,y,z);
    expected = "This is an Equilateral Triangle.";

    if (actual.compare(expected) != 0){
        success = false;
    }

    return ( success );
}

bool testForIsoscelesTriangle(const TriangleTest &uut, int x, int y, int z) {
    /***************************************************************************
     * Function:  testForScaleneTriangle()
     * Purpose:   tests to see if the values passed are similar to Isosceles
     * Inputs:    referenced object, with 3 integer value
     * Outputs:   boolean result (True|False)
     **************************************************************************/
    bool success = true;
    std::string actual;
    std::string expected;

    actual = uut.printResult(x,y,z);
    expected = "This is an Isosceles Triangle.";

    if (actual.compare(expected) != 0){
        success = false;
    }

    return ( success );
}

bool testForScaleneTriangle(const TriangleTest &uut, int x, int y, int z) {
    /***************************************************************************
     * Function:  testForScaleneTriangle()
     * Purpose:   tests to see if the values passed are similar to Scalene
     * Inputs:    referenced object, with 3 integer value
     * Outputs:   boolean result (True|False)
     **************************************************************************/
    bool success = true;

    std::string actual;
    std::string expected;

    actual = uut.printResult(x,y,z);
    expected = "This is an Scalene Triangle.";

    if (actual.compare(expected) != 0){
        success = false;
    }

    return ( success );
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int main() {
    /***************************************************************************
     * Function: main()
     * Purpose:  provides the main execution point for this test suite
     * Inputs:   None
     * Outputs:  integer result: 0 = success, 1 = error
     **************************************************************************/
    TriangleTest TT;

    int x = 0;
    int y = 0;
    int z = 0;

    std::system ("clear");

    // VALID INPUT - Nominal Test: All input values are equal
    x = 1, y = 1, z = 1;
    if (testForEquilateralTriangle( TT, x, y ,z ) == true){
        std::cout << "TESTCASE :: PASS :: testForEquilateralTriangle()" << std::endl;
    }

    // VALID INPUT - Nominal Test: Only 2 input values are equal
    x = 1, y = 1, z = 2;
    if ( testForIsoscelesTriangle(TT, x, y, z) == true ){
        std::cout << "TESTCASE :: PASS :: testForIsoscelesTriangle()" << std::endl;
    }

    // VALID INPUT - Nominal Test: No input values are equal
    x = 1, y = 2, z = 3;
    if (testForScaleneTriangle(TT, x, y, z) == true ){
        std::cout << "TESTCASE :: PASS :: testForScaleneTriangle()" << std::endl;
    }

    // INVALID INPUT - Off-Nominal Test: x-input <= 0
    x = 0, y = 1, z = 1;
    if (testForInvalidInput_X( TT, x, y ,z ) == true){
        std::cout << "TESTCASE :: PASS :: testForInvalidInput_X()" << std::endl;
    }

    // INVALID INPUT - Off-Nominal Test: y-input <= 0
    x = 1, y = 0, z = 1;
    if ( testForInvalidInput_Y(TT, x, y, z) == true ){
        std::cout << "TESTCASE :: PASS :: testForInvalidInput_Y()" << std::endl;
    }

    // INVALID INPUT - Off-Nominal Test: z-input <= 0
    x = 1, y = 1, z = 0;
    if (testForInvalidInput_Z(TT, x, y, z) == true ){
        std::cout << "TESTCASE :: PASS :: testForInvalidInput_Z()" << std::endl;
    }

    return ( 0 );
}
