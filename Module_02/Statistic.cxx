// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  NAME:       Statistic.cxx
//
//  PROJECT:    [ Module 2 ] Statistical Class
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    To implement a class that can accumulate information about a
//              sequence of numbers and calculate its average and Standard
//              Deviation (STD).
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  10 February 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#include <math.h>
#include <cmath>
#include <vector>
#include <iostream>

#include "Statistic.hh"

Statistic::Statistic() {
    /***************************************************************************
     * Method:  Statistic()::public
     * Purpose: default constructor for the class
     * Inputs:  None
     * Outputs: Nothing
     **************************************************************************/
    std::vector<double> input;

}    // end method

Statistic::~Statistic() {
    /***************************************************************************
     * Method:  ~Statistic()::public
     * Purpose: defult destructor for the class
     * Inputs:  None
     * Outputs: Nothing
     **************************************************************************/
}    // end method

void Statistic::add(double x) {
    /***************************************************************************
     * Method:  add()::public
     * Purpose: user interface to accept values for StdDev.
     * Inputs:  1 input value
     * Outputs: Nothing.
     **************************************************************************/
    this->input.push_back(x);

}    // end method

double Statistic::average() const {
    /***************************************************************************
     * Method:  average()::public
     * Purpose: returns the average of values passed into class instance.
     * Inputs:  None.
     * Outputs: average of values passed.
     **************************************************************************/
    const double result = this->getAverage();

    return ( result );
}    // end method

double Statistic::STD() const {    // @suppress("Name convention for function")
    /***************************************************************************
     * Method:  STD()::public
     * Purpose: to provide result of StdDev calculations upon request.
     * Inputs:  None.
     * Outputs: result of StdDev calculations.
     **************************************************************************/
    double result = this->getStdDev();

    return ( result );
}    // end method

double Statistic::getAverage() const {
    /***************************************************************************
     * Method:  getAverage()::private
     * Purpose: calculates the average of values passed into class instance.
     * Inputs:  None.
     * Outputs: average of the numbers passed
     **************************************************************************/
    const double dividen = this->input.size();
    double divisor = 0.00;

    for (int i = 0; i < dividen; i++) {
        divisor += this->input[i];
    }

    return ( divisor / dividen );
}    // end method

double Statistic::getStdDev() const {
    /***************************************************************************
     * Method:  getStdDev()::private
     * Purpose: calculates the standard deviation of values passed.
     * Inputs:  None.
     * Outputs: standard deviation result
     **************************************************************************/
    double sigma = 0.00;
    double x = 0.00;
    const double x_bar = this->getAverage();

    int dof = ( this->input.size() - 1 );       // degrees of freedom

    for (int i = 0; i <= dof; i++) {
        x = this->input[i];
        sigma += pow(( x - x_bar ), 2);         // Sigma = ( x - x-average)^2
    }

    const double result = sqrt(sigma / dof);    // STD =  Square Root (Sigma/DoF)

    return ( result );
}    // end method
