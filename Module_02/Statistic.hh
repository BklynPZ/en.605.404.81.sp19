// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  NAME:       Statistic.hh
//
//  PROJECT:    [ Module 2 ] Statistical Class
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    To define a class that can accumulate information about a
//              sequence of numbers and calculate its average and Standard
//              Deviation (STD).
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  10 February 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#include <vector>

#ifndef STATISTIC_H
#define STATISTIC_H

class Statistic
{
    /***************************************************************************
     * Class:   Statistic()::public
     * Purpose: To accept values and return the StdDev and/or Average of values
     *          values passed.  StdDev and/or Average is returned upon request.
     **************************************************************************/
    public:
        Statistic();        // default constructor
        ~Statistic();       // virtual destructor (over-kill for this program)

        // prototyped "external interface" member functions
        void add(double x);
        double average() const;
        double STD() const;

    private:
        // data members
        std::vector<double> input;

        // prototyped "internal" member functions
        double getAverage() const;
        double getStdDev() const;
};
// end class
#endif // STATISTIC_H
