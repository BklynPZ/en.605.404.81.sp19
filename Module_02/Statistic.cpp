// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//
//  NAME:       Statistic.cpp
//
//  PROJECT:    [ Module 2 ] Statistical Class
//
//  COURSE:     EN.605.404.81.SPR.19
//              Object Oriented Programming Using C++
//
//  PURPOSE:    To provide the main execution point for the Statistical Class.
//
//  AUTHOR:     Patrick N. Stewart
//
//  SUBMITTED:  10 February 2019
//
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <string>
#include "Statistic.hh"

int main(int argc, char *argv[]) {
    /***************************************************************************
     * Function: main()
     * Purpose:  provides the main execution point for this program
     * Inputs:   parameters passed from the console
     * Outputs:  integer result: 0 = success, 1 = error
     **************************************************************************/
    int success = 0;    // set for unfavorable outcome

    Statistic obj;

    if (argc == 2) {

        std::string filename = "";
        std::string line = "";
        int value = 0;

        filename = argv[1];     // setting filename from console input

        std::ifstream handler(filename);

        if (handler) {
            if (handler.is_open()) {
                while (std::getline(handler, line)) {

                    value = atoi(line.c_str()); // converting string to int
                    obj.add(value);             // passing int to class object for calculation
                }
                handler.close();
            }
        }
        else {
            std::cerr
                    << "ERROR: file provide is not accessible by this program!!!"
                    << std::endl;
        }
    }
    else {
        success = 2;    // insufficient inputs
        std::cerr
                << "ERROR: Insufficient inputs provided, we expected a path to a data-file!!!"
                << std::endl;
    }

    std::cout << "Average = " << obj.average() << std::endl;
    std::cout << "StdDev  = " << obj.STD() << std::endl << std::endl;

    std::cout << "Program Done!" << std::endl;    // so we'll know when we're done.

    return ( success );
}
